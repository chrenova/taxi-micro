import asyncio
import logging
import websockets

from realtime_client_data_common.channels import ChannelSubscriber
from realtime_client_data_common import constants
from realtime_client_data_common.publisher import create_incoming_messages_channel_publisher
import auth

logging.basicConfig(level=logging.INFO)

async def websocket_listener(ws, user_id):
    global pub
    try:
        await pub.publish({'user_id': user_id, 'type': constants.MESSAGE_TYPE_CLIENT_CONNECTED})
        while True:
            data = await ws.recv()
            logging.debug(f'Websocket received: {data}')
            await pub.publish({'user_id': user_id, 'type': constants.MESSAGE_TYPE_CLIENT_SENT_DATA, 'data': data})
    except Exception as e:
        logging.info(f'Exception: {e}')
    finally:
        await pub.publish({'user_id': user_id, 'type': constants.MESSAGE_TYPE_CLIENT_DISCONNECTED})
        logging.info('Unregistered')
    logging.info('Websocket listener finished')


class UserChannelSubscriber(ChannelSubscriber):
    def __init__(self, name, ws):
        self.ws = ws
        super().__init__(name)
    async def on_message(self, message):
        await self.ws.send(message)

async def handle_websocket(websocket, path):
    logging.info('Client websocket connection established')
    # TODO authenticate client
    try:
        user_id, role = auth.authenticate(websocket.request_headers)
    except auth.AuthError:
        logging.info('Invalid token, closing connection')
        await websocket.close()
        return

    channel_name = f'{constants.OUTGOING_USER_CHANNEL}{constants.USER_ROLE_ADMIN if role == constants.USER_ROLE_ADMIN else user_id}'
    user_channel_subscriber = UserChannelSubscriber(channel_name, websocket)
    tasks = [
        websocket_listener(websocket, user_id),
        user_channel_subscriber.run()
    ]
    await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
    logging.info('Tasks done')
    await user_channel_subscriber.stop()
    logging.info('Client websocket connection closed')


loop = asyncio.get_event_loop()
pub = create_incoming_messages_channel_publisher()
logging.info('Publisher created')
loop.create_task(pub.run())
logging.info('Publisher running')
loop.run_until_complete(
    websockets.serve(handle_websocket, 'localhost', 6789))
logging.info('Websockets serving')
loop.run_forever()
