from setuptools import setup

setup(
    name='taxi-auth',
    description='Auth',
    version='',
    packages=['auth'],
    install_requires=[
        'PyJWT'
    ]
)
