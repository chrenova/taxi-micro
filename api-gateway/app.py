from aiohttp import web
import json
from common.service_registry import TASKS_ENDPOINT, USERS_ENDPOINT
import auth

routes = web.RouteTableDef()


@routes.get('/api/login/')
async def login(request):
    data = await request.json()
    print(data)
    data = json.loads(data)
    username = data['username']
    password = data['password']
    resp = await USERS_ENDPOINT.login(username, password)
    user = json.loads(resp)
    token = auth.generate_token(user)
    return web.json_response({'token': token})


@routes.get('/api/tasks/')
async def get(request):
    # name = request.match_info.get('name', "Anonymous")
    # text = "Hello, " + name
    # data = await request.json()
    # print(data)
    try:
        user_id, role = auth.authenticate(request.headers)
    except auth.AuthError:
        return web.json_response({'result': 'invalid token'}, status=400)

    resp = await TASKS_ENDPOINT.find(user_id)
    print(resp)
    data = resp['data']
    return web.json_response({'result': 'ok', 'data': data})


@routes.post('/api/tasks/')
async def post(request):
    # name = request.match_info.get('name', "Anonymous")
    # text = "Hello, " + name
    # TODO: asyncio.shield
    try:
        user_id, role = auth.authenticate(request.headers)
    except auth.AuthError:
        return web.json_response({'result': 'invalid token'}, status=400)

    data = await request.json()
    resp = await TASKS_ENDPOINT.create(user_id, data)
    print(resp)
    data = resp['data']
    return web.json_response({'result': 'ok', 'data': data})


app = web.Application()
app.add_routes(routes)

web.run_app(app, port=8080)
