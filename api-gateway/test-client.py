import asyncio
import json
import aiohttp


async def main():
    async with aiohttp.ClientSession() as session:
        async with session.get('http://localhost:8080/api/login/',
                               json=json.dumps({'username': 'qwerty', 'password': 'password'})) as response:
            data = await response.json()
            token = data['token']
            print(f'token: {token}')
        headers = {'authorization': token}
        async with session.get('http://localhost:8080/api/tasks/', headers=headers) as response:
            print(await response.json())
        async with session.post('http://localhost:8080/api/tasks/', headers=headers, json=json.dumps({'a': 'a'})) as response:
            print(await response.json())
        async with session.get('http://localhost:8080/api/tasks/', headers=headers) as response:
            print(await response.json())


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.run_until_complete(asyncio.sleep(0))
loop.close()
