from common import http_requests


class Endpoint:
    def __init__(self, host, port, path):
        self.host = host
        self.port = port
        self.path = path

    def get_path(self):
        return f'http://{self.host}:{self.port}{self.path}'


class TasksEndpoint(Endpoint):
    async def create(self, user_id, data):
        return await http_requests.post(self.get_path(), data)

    async def find(self, user_id):
        return await http_requests.get(self.get_path())


class UsersEndpoint(Endpoint):
    async def create(self, data):
        return await http_requests.post(self.get_path(), data)

    async def find(self, data):
        return await http_requests.get(self.get_path())

    async def login(self, username, password):
        from common import database
        import json
        return json.dumps(database.USER_DATABASE[0])

TASKS_ENDPOINT = TasksEndpoint(host='localhost', port=8081, path='/tasks/')
USERS_ENDPOINT = UsersEndpoint(host='localhost', port=8082, path='/users/')
