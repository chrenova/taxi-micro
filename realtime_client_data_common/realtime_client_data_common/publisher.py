from realtime_client_data_common import constants
# from common.queues import Producer
from realtime_client_data_common.channels import ChannelPublisher


#class OutgoingChannelPublisher(ChannelPublisher):
#    def __init__(self):
#        super().__init__()


#def create_outgoing_channel_publisher():
#    return OutgoingChannelPublisher()


class IncomingMessagesChannelPublisher(ChannelPublisher):
    def __init__(self):
        super().__init__(constants.INCOMING_REALTIME_CLIENT_DATA_CHANNEL)


def create_incoming_messages_channel_publisher():
    return IncomingMessagesChannelPublisher()


#class UserChannelPublisher(ChannelPublisher):
#    def __init__(self):
#        super().__init__()


#def create_user_channel_publisher():
#    return UserChannelPublisher()


#class MainMessageQueueProducer(Producer):
#    def __init__(self):
#        super().__init__(constants.APP_MAIN_MESSAGE_QUEUE)

#def create_main_mq_producer():
#    return MainMessageQueueProducer()

#class ApplicationEventsChannelPublisher(channels.ChannelPublisher):
#    def __init__(self):
#        super().__init__(constants.REDIS_COMMON_CHANNEL_APPLICATION_EVENTS)

#def create_application_events_channel_publisher():
#    return ApplicationEventsChannelPublisher()
