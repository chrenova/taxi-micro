import time
import aioredis

schema = {
    'last_connected': time.time(),
    'last_disconnected': time.time(),
    'last_message': time.time(),
    'lat': float(0),
    'lon': float(0)
}

async def connect(user_id):
    conn = await aioredis.create_redis('redis://localhost/0')
    data = {
        'last_connected': time.time()
    }
    await conn.hmset_dict(f'clients:{user_id}', data)
    conn.close()
    await conn.wait_closed()


async def disconnect(user_id):
    conn = await aioredis.create_redis('redis://localhost/0')
    data = {
        'last_disconnected': time.time()
    }
    await conn.hmset_dict(f'clients:{user_id}', data)
    conn.close()
    await conn.wait_closed()


async def update(user_id, data):
    conn = await aioredis.create_redis('redis://localhost/0')
    data = {
        'last_message': time.time()
    }
    await conn.hmset_dict(f'clients:{user_id}', data)
    conn.close()
    await conn.wait_closed()


async def get_clients():
    conn = await aioredis.create_redis('redis://localhost/0', encoding='utf-8')
    clients = {}
    clients_list = await conn.keys('clients:*')
    for cl in clients_list:
        client = await conn.hgetall(cl)
        clients[cl] = client
    conn.close()
    await conn.wait_closed()
    return clients
