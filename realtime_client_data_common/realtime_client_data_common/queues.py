import logging
import aioredis


class Consumer:

    def __init__(self, queue_name):
        self.queue_name = queue_name

    async def run(self):
        await self._create_consumer()

    async def handle_message(self, message):
        pass

    async def _create_consumer(self):
        self.conn = await aioredis.create_redis_pool('redis://localhost')
        await self._consume()

    async def _consume(self):
        while True:
            try:
                _, msg = await self.conn.brpop(self.queue_name)
                from common.events import Event
                ev = Event.from_json(msg)
                await self.handle_message(ev)
            except Exception as e:
                logging.warning(e)
            finally:
                logging.debug('Finally')

    async def close(self):
        self.conn.close()
        await self.conn.wait_closed()


class Producer:

    def __init__(self, queue_name):
        self.queue_name = queue_name

    async def run(self):
        await self._create_producer()

    async def _create_producer(self):
        self.conn = await aioredis.create_redis_pool('redis://localhost')

    async def produce(self, event):
        message = event.to_json()
        try:
            await self.conn.lpush(self.queue_name, message)
            print('Message produced')
        except Exception as e:
            logging.warning(e)
        finally:
            logging.debug('Finally')
