import logging
import asyncio
import json
from realtime_client_data_common import constants, channels, clients

logging.basicConfig(level=logging.INFO)


class IncomingMessagesChannelSubscriber(channels.ChannelSubscriber):
    def __init__(self):
        super().__init__(constants.INCOMING_REALTIME_CLIENT_DATA_CHANNEL)
    async def on_message(self, message):
        logging.info(f'IncomingMessagesChannel received a message: {message}')
        # save data to redis
        data = json.loads(message)
        user_id = data['user_id']
        typ = data['type']
        data = data['data'] if 'data' in data else {}
        if typ == constants.MESSAGE_TYPE_CLIENT_CONNECTED:
            await clients.connect(user_id)
        elif typ == constants.MESSAGE_TYPE_CLIENT_DISCONNECTED:
            await clients.disconnect(user_id)
        elif typ == constants.MESSAGE_TYPE_CLIENT_SENT_DATA:
            await clients.update(user_id, data)


class IncomingMessageListener:
    def __init__(self):
        self.clients = set()
        self.sub = None

    async def run(self):
        self.sub = IncomingMessagesChannelSubscriber()
        tasks = [
            asyncio.ensure_future(self.sub.run())
        ]
        asyncio.wait(tasks)
        logging.info('IncomingMessageListener running')


class UserChannelPublisher(channels.ChannelPublisher):
    def __init__(self):
        super().__init__()


async def send_client_update_notifications(pub):
    """send notification to admin channel"""
    client_list = await clients.get_clients()
    # TODO sort list
    client_message = {
        'type': constants.MESSAGE_TYPE_NOTIFICATION_CLIENT_STATUS,
        'data': client_list
    }
    logging.info(client_message)
    await pub.publish(client_message, constants.OUTGOING_ADMIN_CHANNEL)


async def periodic_task(pub):
    while True:
        await send_client_update_notifications(pub)
        logging.info('Notification sent')
        await asyncio.sleep(10)


def main():
    user_channel_publisher = UserChannelPublisher()
    incoming_message_listener = IncomingMessageListener()
    asyncio.ensure_future(incoming_message_listener.run())
    logging.info('realtime client data listener running')
    asyncio.ensure_future(user_channel_publisher.run())
    logging.info('user channel publisher running')
    asyncio.ensure_future(periodic_task(user_channel_publisher))
    logging.info('periodic task running')


if __name__ == '__main__':
    main()
    asyncio.get_event_loop().run_forever()
