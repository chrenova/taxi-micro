import asyncio
import logging
import aioredis
import json


class ChannelPublisher:

    def __init__(self, name=None):
        self.pub = None
        self.name = name

    async def run(self):
        await self._create_pub()

    async def _create_pub(self):
        self.pub = await aioredis.create_redis('redis://localhost/0')

    async def stop(self):
        self.pub.close()

    async def publish(self, message, name=None):
        destination = name if name else self.name
        message = json.dumps(message)
        await self.pub.publish_json(destination, message)


class ChannelSubscriber:

    def __init__(self, name):
        self.sub = None
        self.channel = None
        self.name = name

    async def run(self):
        await self._create_sub()

    async def _create_sub(self):
        self.sub = await aioredis.create_redis('redis://localhost/0')
        self.channel, = await self.sub.subscribe(self.name)
        logging.info(f'ChannelSubscriber subscribed to channel {self.name}')
        await self._listener()

    async def stop(self):
        await self.sub.unsubscribe(self.name)
        self.sub.close()
        logging.info('ChannelSubscriber finished')

    async def _listener(self):
        while await self.channel.wait_message():
            # TODO add try except to handle non-json messages
            msg = await self.channel.get_json()
            asyncio.ensure_future(self.on_message(msg))
            logging.info(f'Got Message: {msg}')
        logging.info('Redis listener finished')

    async def on_message(self, message):
        pass
