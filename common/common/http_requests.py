import aiohttp

async def get(url):
    print(f'Getting {url}')
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.json()

async def post(url, data):
    print(f'Posting {url} {data}')
    async with aiohttp.ClientSession() as session:
        async with session.post(url, json=data) as response:
            return await response.json()
