from common import database

async def get_tasks():
    return database.TASK_DATABASE

async def get_task(task_id):
    return database.TASK_DATABASE[task_id]

async def create_task(**kwargs):
    task = {}
    for k, v in kwargs.items():
        task[k] = v
    database.TASK_DATABASE.append(task)
    return task

async def claim_task(**kwargs):
    return
