from setuptools import setup

setup(
    name='taxi-common',
    description='Common',
    version='',
    packages=['common'],
    install_requires=[
        'aiohttp',
        'aioredis'
    ]
)
