import jwt
from datetime import datetime, timedelta

JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_SECONDS = 20


class AuthError(Exception):
    def __init__(self, arg):
        self.args = arg


def generate_token(user):
    payload = {
        'user_id': user['id'],
        'role': user['role'],
        'exp': datetime.utcnow() + timedelta(seconds=JWT_EXP_DELTA_SECONDS)
    }
    token = jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM)
    return token.decode('utf-8')


def verify_token(token):
    if not token:
        raise AuthError('Token is null')
    else:
        try:
            payload = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
            return payload['user_id'], payload['role']
        except (jwt.DecodeError, jwt.ExpiredSignatureError) as e:
            raise AuthError(e)


def authenticate(headers):
    """raises auth.AuthError"""
    jwt_token = headers.get('authorization', None)
    user_id, role = verify_token(jwt_token)
    print(f'user_id: {user_id}, role: {role}')
    return user_id, role
