from aiohttp import web
import json
import logging
import services
from common.config import USE_REALTIME_CLIENT_DATA

if USE_REALTIME_CLIENT_DATA:
    from realtime_client_data_common.publisher import create_incoming_messages_channel_publisher
    from realtime_client_data_common import constants as realtime_constants
    pub = create_incoming_messages_channel_publisher()

routes = web.RouteTableDef()

async def notify(typ, data):
    if USE_REALTIME_CLIENT_DATA:
        await pub.publish({'type': typ, 'data': data})


@routes.get('/tasks')
async def get(request):
    # name = request.match_info.get('name', "Anonymous")
    # text = "Hello, " + name
    results = await services.get_tasks()
    data = {'data': results}
    return web.json_response(data)


@routes.post('/tasks')
async def post(request):
    # TODO process headers (request token)
    # name = request.match_info.get('name', "Anonymous")
    # text = "Hello, " + name
    data = await request.json()
    data = json.loads(data)
    # TODO catch exceptions
    results = await services.create_task(**data)
    # TODO implement
    await notify(realtime_constants.MESSAGE_TYPE_TASK_CREATED, None)
    data = {'data': results}
    return web.json_response(data)


@routes.put('/tasks/{task_id}/claim')
async def put(request):
    task_id_str = request.match_info.get('task_id', None)
    if not task_id_str:
        # TODO return value
        return
    task_id = int(task_id_str)
    result = await services.claim_task()
    await notify(realtime_constants.MESSAGE_TYPE_TASK_CLAIMED, None)
    data = {'data': result}
    return web.json_response(data)


app = web.Application()
app.add_routes(routes)

if USE_REALTIME_CLIENT_DATA:
    loop = app.loop
    loop.create_task(pub.run())
    logging.info('Using realtime data')
    logging.info('Publisher running')
else:
    logging.info('Not using realtime data')

web.run_app(app, port=8081)
