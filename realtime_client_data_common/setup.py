from setuptools import setup

setup(
    name='realtime_client_data_common',
    description='Common',
    version='',
    packages=['realtime_client_data_common'],
    install_requires=[
        'aioredis'
    ]
)
