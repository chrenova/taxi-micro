import asyncio
import json
import aiohttp


async def main():
    async with aiohttp.ClientSession() as session:

        headers = dict()
        async with session.get('http://localhost:8080/api/login/',
                               json=json.dumps({'username': 'qwerty', 'password': 'password'})) as response:
            data = await response.json()
            token = data['token']
            print(f'token: {token}')
            headers = {'authorization': token}

        async with session.ws_connect('http://localhost:6789', headers=headers) as ws:
            await ws.send_json({'user_id': 0})
            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.TEXT:
                    if msg.data == 'close cmd':
                        await ws.close()
                        break
                    else:
                        print(msg.data)
                        # await ws.send_str(msg.data + '/answer')
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    break


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.run_until_complete(asyncio.sleep(0))
loop.close()
